package com.example.projectspring.controller;

import com.example.projectspring.model.StudentModel;
import com.example.projectspring.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @GetMapping("/{id}")
    public Optional<StudentModel> getStudent(@PathVariable int id){
        return studentService.getStudent(id);
    }
    @PostMapping()
    public StudentModel addStudent(@RequestBody StudentModel student){
        return studentService.addStudent(student);
    }
    @PatchMapping("update/{id}")
    public String updateStudent(@RequestBody StudentModel s , @PathVariable int id){
        return studentService.updateStudent(id);


    }
    @DeleteMapping("/delete/{id}")
    public String deleteStudent(@PathVariable int id){
        return studentService.deleteStudent(id);

    }
}