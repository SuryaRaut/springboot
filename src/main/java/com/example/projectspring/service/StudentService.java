package com.example.projectspring.service;

import com.example.projectspring.model.StudentModel;
import com.example.projectspring.model.UpdateStudentRequest;
import com.example.projectspring.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepo studentRepo;

    public StudentModel getStudent(int studentId){
       return studentRepo.findById(studentId) ;
    }
    public StudentModel addStudent(StudentModel student){
        return studentRepo.save(student);
    }
    public StudentModel updateStudent(int studentId, UpdateStudentRequest request){
        StudentModel student = studentRepo.findById(studentId);
        String name = request.getName();
        String email = request.getEmail();
        if(name!=null){
            student.setEmail(email);
        }
        if(email!=null){
            student.setName(name);
        }
        return studentRepo.save(student);
    }
    public String deleteStudent(int id){
        if(studentRepo.existsById(id)){
            studentRepo.deleteById(id);
            return "Item Deleted";
        }
        return "Id Doesn't Exist";
    }


}